# react-komenci-cli

command line tool to help you bootstrap react applications with minimum dependencies for building real world applications.

This command line tool is a wrapper around the [react-komenci](https://gitlab.com/abreu.marcos/react-komenci) project.

## Features

### Development Features
- [x] [auto transpilation](https://babeljs.io/) - Development using modern JavaScript
- [x] [auto compilation](https://webpack.github.io/) - Compile code for development and production automatically
- [x] [hot reloading](https://github.com/webpack/docs/wiki/hot-module-replacement-with-webpack) - See the changes as you edit your source files
- [x] [sync browsers](https://www.browsersync.io/) - Sync multiple browsers for simple visual test
- [x] [unit tests](https://mochajs.org/) - Automatically run unit tests every time you save a file
- [x] [code linting](http://eslint.org/) - Make sure your code is following a defined standard

### Production Features

- [x] **css framework** - base your site styles on solid css frameworks
- [x] **source maps** - serve compiled files, but be able to debug using the source code you developed
- [x] **caching buster** - assets are cached using a content hash for better http trafic

The following production features are **NOT** included by default, but can be easly added as shown on the usage section.

- [x] **Authentication (auth)**
- [x] **Localization (l10n)**
- [x] **Internationalization (i18n)**

## Installation

```
npm install -g react-komenci-cli
```

After you install this npm package you will have access to the `rk` command.

It is recommended that you install this tool globally, in order to have the `rk` command available anywhere on your system.


## Usage

### Bootstrapping a ReactJS Application

```
rk [project-name] [options]
```

This command bootstrap a react project with the minimum dependencies possible. Adding a demo site to showcase the features available on the project.

#### Options

- `-a [or --auth]`      - adds the Authentication modules (and modify the demo site to showcase the feature)
- `-l [or --locale]`    - adds the Locale / Internationalization modules (and modifies the demo site to showcase the feature)
- `-d [or --no-demo]`   - removes the demo site from the application
- `-b [or --bootstrap]` - switch the css framework to use [bootstrap](http://getbootstrap.com/) (by default it uses [zurb foundation](http://foundation.zurb.com/))

#### Examples


**Default React**

```
rk [project-name]
```

bootstrap a default react project with the minimum dependencies possible. It also adds a simple demo site.

**React + Authentication**

```
rk [project-name] --auth
```

bootstrap a react project with all the required modules necessary for user Authentication. It also adds auth features to the demo site.

**React + Localization/Internationalization**

```
rk [project-name] --locale
```

bootstrap a react project with all the required modules necessary for working with Localization and Internationalization. It also adds these features to the demo site.

**React + No Demo Site**

```
rk [project-name] --auth --locale --no-demo
```

bootstrap a react project with all required modules necessary for user Authentication as well as Localization/Internationalization. But the `--no-demo` removes the demo site.

**React + Foundation**

```
rk [project-name]
```

by default the project created with the `rk` command line, will include the [zurb foundation](http://foundation.zurb.com/) css framework

**React + Bootstrap**

```
rk [project-name] --bootstrap
```

bootstrap a react project using [bootstrap](http://getbootstrap.com/) as the css framework