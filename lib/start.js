const path        = require('path');
const exec        = require('child_process').exec;
const request     = require('request');
const fs          = require('fs');
const Promise     = require('bluebird');
const tmp         = require('tmp');
const AdmZip      = require('adm-zip');
const chalk       = require('chalk');

const addAuth           = require('./utils/add-auth');
const addLocale         = require('./utils/add-locale');
const addAuthLocale     = require('./utils/add-auth-locale');
const removeDemo        = require('./utils/remove-demo');
const addTemplates      = require('./utils/add-templates');
const switchToBootstrap = require('./utils/switch-to-bootstrap');
const logger            = require('./utils/logger');
const configProject     = require('./utils/config-project');


const cmdGetLastTag = 'git ls-remote --tags https://gitlab.com/abreu.marcos/react-komenci | cut -c52- | grep -v { | grep -v - | grep -v .rc | grep -v .pre | grep -v 0pre | sort -n -t . -k 1,1n -k 2,2n -k 3,3n | tail -1';
const reactKomenciArchive = 'https://gitlab.com/abreu.marcos/react-komenci/repository/archive.zip';

/*
 * get the latest released tag from remote `react-komenci` project
 */
const lastestTagVersion = () => {
  logger.log('verifying latest tag from remote `react-komenci`');

  return new Promise(function(resolve, reject) {
    exec(cmdGetLastTag, function(error, stdout, stderr) {
      if (error) {
        return reject('unable get last tag from `react-komenci` - check your internet connection and make sure you have git installed');
      }
      return resolve(stdout);
    });
  })
};

const downloadTag = (version) => {
  logger.log('downloading `react-komenci` tag: ' + version);

  const destObj = tmp.fileSync({postfix: '.zip'});

  return new Promise(function(resolve, reject) {
    const file = fs.createWriteStream(destObj.name);
    const url = reactKomenciArchive + '?ref=' + version;

    // generic error handling method
    const handleError = err => {
      fs.unlink(destObj.name); // delete file | async call, but no need to wait for the result
      reject('error downloading `react-komenci` tag: ' + version + '\n' + err.message);
    };

    // start the request for the informed tag version
    request.get(url)
      .on('response', response => {
        // treat non 200 responses as errors
        if (response.statusCode !== 200) {
          reject('`react-komenci` tag (' + version + ') download failed with status code: ' + response.statusCode);
        }
      })
      .on('error', err => handleError(err))
      .pipe(file)
      .on('close', () => resolve(destObj.name));
  });
};

const validatePaths = (appName, zipPath) => {
  const destPath = path.join(process.env.PWD, appName);
  return new Promise(function(resolve, reject) {
    try {
      fs.accessSync(zipPath, fs.F_OK);
    } catch(e) { return reject('downloaded zip file inaccessible at: ' + zipPath); }

    var content;
    try {
      content = fs.readdirSync(destPath);
    }
    catch(e) {
      if (e.code === 'ENOENT') {
        try {
          fs.accessSync(path.dirname(destPath), fs.F_OK);
        } catch(e) { return reject('parent destinaltion folder inaccessible'); }
        return resolve(destPath);
      }
    }

    if (content.length > 0) {
      return reject('application folder exists and is not empty');
    }

    return resolve(destPath);
  });
};


/*
 * extracts a zip file and return the first subfolder path
 */
const extractPackage = (zipPath) => {
  const zip = new AdmZip(zipPath);
  const tmpObj = tmp.dirSync();

  zip.extractAllTo(tmpObj.name);

  return new Promise(function(resolve, reject) {
    fs.readdir(tmpObj.name, function(err, filenames) {
      if (err) {
        return reject(err);
      }

      if (filenames.length > 1) {
        return reject('expected only one item when extracting zip file, but found ' + filenames.length);
      }
      return resolve(path.join(tmpObj.name, filenames[0]));
    });
  });
};

const start = (appName, options) => {
  return lastestTagVersion()
  .then(downloadTag)
  .then(function(filePath) {
    return validatePaths(appName, filePath)
    .then(function(destPath) {
      return { filePath, destPath };
    });
  })
  .then(function(info) {
    return extractPackage(info.filePath)
    .then(function(extractedPath) {
      info.extractedPath = extractedPath;
      return info;
    });
  })
  .then(function(info) {
    fs.renameSync(info.extractedPath, info.destPath);
    delete info.extractedPath;
    return info;
  })
  .then(function(info) {
    return configProject(appName, info).then(() => {
      console.log('\n');
      return info;
    });
  })
  .then(function(info) {
    if (options.auth && options.locale) {
      return addAuthLocale(info.destPath, options.demo).then(() => info);
    }
    else if (options.auth) {
      return addAuth(info.destPath, options.demo).then(() => info);
    }
    else if (options.locale) {
      return addLocale(info.destPath, options.demo).then(() => info);
    }
    else {
      return addTemplates(info.destPath, 'no-demo').then(() => info);
    }
  })
  .then(function(info) {
    if (!options.demo) {
      return removeDemo(info.destPath, options).then(() => info);
    }
    return info;
  })
  .then(function(info) {
    if (options.bootstrap) {
      return switchToBootstrap(info.destPath, options.demo).then(() => info);
    }
    return info;
  })
  .then(function(info) {
    console.log('\n\n', 'Your application ' + appName + ' is fully setup');
    console.log('\n\n', chalk.green('=== Development ==============================='));
    console.log(' to start developing - run the following commands: ');
    console.log(chalk.cyan('    cd ' + info.destPath));
    console.log(chalk.cyan('    npm install'));
    console.log(chalk.cyan('    npm start'));
    console.log('\n\n', chalk.green('=== Production ================================'));
    console.log(' to prepare your code for production deployment run the following commands: ');
    console.log(chalk.cyan('    cd ' + info.destPath));
    console.log(chalk.cyan('    npm install'));
    console.log(chalk.cyan('    npm run build'));
    console.log('\n\n', chalk.green('=== Help ======================================'));
    console.log(' to verify the npm commands available run: ');
    console.log(chalk.cyan('    cd ' + info.destPath));
    console.log(chalk.cyan('    npm install'));
    console.log(chalk.cyan('    npm run help'));
    console.log('\n\n');
  });
};

module.exports = start;
