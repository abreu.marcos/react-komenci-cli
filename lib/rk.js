#!/usr/bin/env node
const updateNotifier  = require('update-notifier');
const program         = require('commander');

const start           = require('./start');
const pkg             = require('../package.json');
const logger          = require('./utils/logger');

// check if a new version of rk is avaialble and print an update notification
updateNotifier({pkg: pkg})
  .notify({defer: false});

program
  .version(pkg.version);

program
  .arguments('<app-name>')
  .description('bootstrap a react application')
  .usage('<app-name> [options]')
  .option('-a, --auth', 'adds the authentication modules')
  .option('-l, --locale ', 'adds the localization/internationalization modules')
  .option('-d, --no-demo', 'removes the demo site from the application')
  .option('-b, --bootstrap', 'switch the css framework to use bootstrap instead of foundation')
  .option('-v, --verbose', 'logs step by step as the command executes')
  .action(function(name, cmd) {
    const options = {
      auth: !!cmd.auth,
      locale: !!cmd.locale,
      demo: !!cmd.demo,
      bootstrap: !!cmd.bootstrap,
      verbose: !!cmd.verbose
    };

    // initialize the logger
    logger.init(options);

    // execute the command
    start(name, options)
    .then(function() {
      process.exit(0);
    })
    .catch(function(err) {
      logger.error(err);
      process.exit(1);
    });
  });

program.cli = true;
program.parse(process.argv);

if (!program.args.length || !program.args[1]) {
  console.error('invalid rk command - check the help bellow to see proper usage:');
  program.help();
  process.exit(1);
}
