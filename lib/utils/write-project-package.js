const fs      = require('fs');
const Promise = require('bluebird');

const writeProjectPackage = (pkgPath, pkgObject) => {
  const newPkg = JSON.stringify(pkgObject, null, 2);
  return new Promise(function(resolve, reject) {
    fs.writeFile(pkgPath, newPkg, (err) => {
      if (err) {
        return reject('failed configuring the project\n' + err);
      }
      return resolve();
    });
  });
};

module.exports = writeProjectPackage;
