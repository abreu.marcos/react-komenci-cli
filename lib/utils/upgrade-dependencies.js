const fs   = require('fs');
const path = require('path');
const ncu  = require('npm-check-updates');
const _    = require('lodash');

const writeProjectPackage = require('./write-project-package');

/*
 * upgraded dependencies from a nodejs project with the informed dependencies
 */
const upgradeDependencies = (appPath, dependencies) => {
  const pkgPath = path.resolve(appPath, 'package.json');
  const pkg = require(pkgPath);

  var newDependencies = Object.assign({}, dependencies, pkg.dependencies);
  // replace with new list of dependencies
  pkg.dependencies = newDependencies;

  // upgrade dependencies
  return ncu.run({
    packageData: JSON.stringify(pkg),
    loglevel: 'silent',
    silent: true,
    jsonAll: true
  })
  // write back to the package.json file
  .then(function(upgraded) {
    return writeProjectPackage(pkgPath, upgraded);
  })
  .then(function() {
    return appPath;
  });
};

module.exports = upgradeDependencies;
