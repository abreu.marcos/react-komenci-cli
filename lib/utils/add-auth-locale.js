const Promise = require('bluebird');

const upgradeDependencies = require('./upgrade-dependencies');
const addTemplates = require('./add-templates');

// list of auth and l10n/i18n dependencies - using old version that will be later upgraded
const dependencies = {
  'bluebird'            : '3.4.1',
  'intl'                : '1.2.4',
  'js-cookie'           : '2.1.2',
  'object-assign'       : '4.1.0',
  'react'               : '15.2.0',
  'react-dom'           : '15.2.0',
  'react-intl'          : '2.1.3',
  'react-redux'         : '4.4.5',
  'react-router'        : '2.5.2',
  'react-router-redux'  : '4.0.5',
  'redux'               : '3.5.2',
  'redux-saga'          : '0.11.0'
};

/*
 * add auth and l10n/i18n feature to the application, by configuring it
 *     and optionally adding a demo to showcase the feature
 */
const addAuthLocale = (appPath, hasDemo) => {
  return addTemplates(appPath, 'auth-locale')
    .then(() => {
      return upgradeDependencies(appPath, dependencies);
    });
};

module.exports = addAuthLocale;
