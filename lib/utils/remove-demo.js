const fs   = require('fs');
const path = require('path');

const addTemplates = require('./add-templates');

const removeFiles = (folderPath, extensions) => {
  var files;
  extensions = extensions || ['.js', '.json'];

  try {
    files = fs.readdirSync(folderPath);
  } catch(e) { return; }

  if (files.length > 0) {
    files.forEach(function(file) {
      const filePath = path.join(folderPath, file);
      if (fs.statSync(filePath).isFile()) {
        if (extensions.indexOf(path.extname(file)) >= 0) {
          fs.unlinkSync(filePath);
        }
      }
      else {
        removeFiles(filePath, extensions);
      }
    });
  }
};

const removeDemo = (appPath, options) => {
  removeFiles(path.join(appPath, 'src', 'components'));
  removeFiles(path.join(appPath, 'src', 'containers'));
  removeFiles(path.join(appPath, 'src', 'lib'));
  removeFiles(path.join(appPath, 'src', 'locales'));
  removeFiles(path.join(appPath, 'src', 'assets', 'images'), ['.png', '.jpg', '.jpeg', '.gif', '.ico', '.svg']);
  removeFiles(path.join(appPath, 'src', 'state'));

  return addTemplates(appPath, 'no-demo');
};

module.exports = removeDemo;
