const path = require('path');
const cpy  = require('cpy');

/*
 * add templates by recursively copying the appropriate files over to the application
 *     keep folder structure
 */
const addTemplates = (appPath, templateFolder) => {
  return cpy(['**/*'], path.join(appPath, 'src'), {
    cwd: path.resolve(__dirname, '../../templates/' + templateFolder + '/'),
    parents: true,
    nodir: true
  })
  .then(function() {
    return appPath;
  });
};

module.exports = addTemplates;
