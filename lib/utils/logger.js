const chalk = require('chalk');
var verbose;

const log = (msg) => {
  if (verbose) {
    console.log(msg);
  }
};

const error = (msg) => {
  console.error(chalk.red('Error: ') + msg, '\n');
};

const init = (options) => {
  verbose = options.verbose;
};

module.exports = {
  log: log,
  error: error,
  init: init
};
