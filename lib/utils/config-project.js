const path        = require('path');
const inquirer    = require('inquirer');

const writeProjectPackage = require('./write-project-package');

const questions = [
  {
    name: 'description',
    message: 'description:'
  },
  {
    name: 'version',
    message: 'version:',
    default: '1.0.0'
  },
  {
    name: 'repo',
    message: 'git repository:'
  },
  {
    name: 'license',
    message: 'license:',
    default: 'MIT'
  }
];

const configProject = (appName, info) => {
  console.log('\n', 'configure your project: ', '\n');
  return inquirer.prompt(questions)
  .then((answers) => {
    const pkg = require(path.resolve(info.destPath, 'package.json'));
    pkg.name = appName;
    pkg.description = answers.description;
    pkg.version = answers.version;
    pkg.author = void 0;
    pkg.private = void 0;
    pkg.engines = void 0;
    pkg.repository = (answers.repo ? { type: 'git', url: 'git+' + answers.repo } : void 0);
    pkg.license = answers.license;
    pkg.keywords = ["react", "reactjs", "react-router", "hot", "reload", "hmr", "webpack"];

    return pkg;
  })
  .then(function(pkg) {
    return writeProjectPackage(path.resolve(info.destPath, 'package.json'), pkg);
  });
};

module.exports = configProject;
