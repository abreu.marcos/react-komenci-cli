import Cookies from 'js-cookie';

/**
 * attempt to infer the prefered language to see the application
 */
export const getLocale = (global, defaultLocale) => {
  let locale;

  // search on the global object
  if (global.locale || global.lang) {
    locale = global.locale || global.lang;
  }

  // search on the querystring
  else if (global.location.search) {
    const qsLang = global.location.search.match(/lang=([^&]*)/);
    if (qsLang[1]) {
      locale = qsLang[1];
    }
  }

  // read from cookies
  else if (Cookies.get('lang') || Cookies.get('locale')) {
    locale = Cookies.get('lang') || Cookies.get('locale');
  }

  // use the browser knowledge of the user language
  //   this method returns the browser language not the user prefered language,
  //   but in most cases is still a good guess
  else if (global.navigator.userLanguage || global.navigator.language) {
    locale = global.navigator.userLanguage || global.navigator.language;
  }

  if (locale) {
    return locale.split('-')[0];
  }
  else {
    return defaultLocale;
  }
};
