import Promise from 'bluebird';

export const loadLocaleData = locale => {
  let messages;
  if (locale === 'fr') {
    messages = require('../../locales/fr.json');
  }
  else {
    messages = require('../../locales/en.json');
  }
  return Promise.resolve({ locale, messages });
};
