import React from 'react';
import { render } from 'react-dom';


// ### Static Global Assets
// ----------------------------------------------------------------------------
import './assets/images/favicon.ico'; // webpack to load favicon.ico
import './assets/styles/app.scss'; // root SASS style tree


// ### Routes
// ----------------------------------------------------------------------------
import { Router, browserHistory } from 'react-router';
import routes from './routes';


// ### Redux (application state) and Sagas (asynchrounous flows)
// ----------------------------------------------------------------------------
import { Provider } from 'react-redux';
import sagas from './state/sagas';
import createSagaMiddleware from 'redux-saga';
import configureStore from './state/store/configure-store'; // eslint-disable-line import/default
const sagaMiddleware = createSagaMiddleware();
const store = configureStore(undefined, sagaMiddleware);
sagaMiddleware.run(sagas);


// ### Browser History
// ----------------------------------------------------------------------------\
import { syncHistoryWithStore } from 'react-router-redux';
const history = syncHistoryWithStore(browserHistory, store); // syncs history with store


// ### Internationalization (l18n) + Localization (l10n)
// ----------------------------------------------------------------------------
import { addLocaleData } from 'react-intl';
import L18nProvider from './containers/l18n-provider';
// for each supported language import the related locale data here
import en from 'react-intl/locale-data/en';
import fr from 'react-intl/locale-data/fr';
const defaultLocale = 'en';
// load each supported language
addLocaleData([...en, ...fr]);

// When not avaialble *cough*Safari*cough*IE<11*cough* inject polyfill with locale data
// It is important to load all supported languages (not only the current language)
if (!window.Intl) {
  require.ensure([
    'intl',
    'intl/locale-data/jsonp/en.js',
    'intl/locale-data/jsonp/fr.js'
  ], function (require) {
    window.Intl = require('intl');
    require('intl/locale-data/jsonp/en.js');
    require('intl/locale-data/jsonp/fr.js');
    runApp();
  }, 'intl-bundle');
} else {
  runApp();
}

import { getLocale } from './lib/locale';


// ### Application Bootstrap
// ----------------------------------------------------------------------------
import Promise from 'bluebird';
import * as api from './lib/api';

function runApp() {
  // bootstrap the app by getting the set locale
  // and then fetching locale data
  // and then rendering the react components
  Promise.resolve(getLocale(window, defaultLocale))
  .then(api.loadLocaleData)
  .then(function({locale, messages}) {
    render(
      <Provider store={store}>
        <L18nProvider initialLocale={locale} initialMessages={messages} defaultLocale={defaultLocale}>
          <Router history={history} routes={routes} />
        </L18nProvider>
      </Provider>, document.getElementById('app')
    );
  })
  .catch(function() {
    // todo: dev environment should log the issue
    // todo: prod environment should log the issue to a log server
  });
}


// ### Expose Methods
// ----------------------------------------------------------------------------

// expose the store to other modules, allowing non-components to access it
export function getStore() {
  return store;
}
