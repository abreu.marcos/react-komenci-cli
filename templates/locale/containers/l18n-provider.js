import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';

import { switchLanguage } from '../state/actions/locale';

// This class will wrap its children in a connected IntlProvider, so that when the
// locale state changes react will re-render all necessary children components
class L18nProvider extends React.Component {
  static propTypes = {
    initialLocale: PropTypes.string,
    locale: PropTypes.string.isRequired,
    initialMessages: PropTypes.object,
    defaultLocale: PropTypes.string.isRequired,
    messages: PropTypes.object.isRequired,
    switchLanguage: PropTypes.func.isRequired,
    children: PropTypes.element
  }

  render() {
    let locale;
    let messages;

    if (Object.keys(this.props.messages).length > 0) {
      locale = this.props.locale;
      messages = this.props.messages;
    }
    else {
      locale = this.props.initialLocale;
      messages = this.props.initialMessages;
      if (locale !== this.props.defaultLocale) {
        this.props.switchLanguage(locale);
      }
    }

    return (
      <IntlProvider locale={locale} key={locale} messages={messages}>
        {this.props.children}
      </IntlProvider>
    );
  }
}

function mapStateToProps(state) {
  const { locale, messages } = state.locale;
  return { locale, messages };
}

const mapActionsToProps = dispatch => {
  return bindActionCreators({ switchLanguage }, dispatch);
};

export default connect(mapStateToProps, mapActionsToProps)(L18nProvider);
