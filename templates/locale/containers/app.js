import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { intlShape, injectIntl } from 'react-intl';

import * as localeActionCreators from '../state/actions/locale';

import AppHeader from '../components/app-header';
import AppFooter from '../components/app-footer';

/*
 * App Component - Root component of our application
 *                 include here the shared functionality of the app
 */
class App extends Component {

  // component properties
  static propTypes = {
    locale: PropTypes.string.isRequired,
    switchLanguageRequest: PropTypes.func.isRequired,   // dispatch switchLanguage action
    intl: intlShape.isRequired,
    children: PropTypes.element             // element children
  }

  /**
   * Render App Shared Components
   */
  render() {
    const { locale, switchLanguageRequest, intl, children } = this.props;
    return (
      <div className="expanded row column">
        <AppHeader
          locale={locale}
          switchLanguage={switchLanguageRequest}
          intl={intl} />

          {children}

        <AppFooter />
      </div>
    );
  }
}

// link app state with component properties
const mapStateToProps = state => ({
  locale: state.locale.locale
});

// link app action creators with component properties
const mapActionsToProps = dispatch => {
  return bindActionCreators({
    switchLanguageRequest: localeActionCreators.switchLanguageRequest
  }, dispatch);
};

export default connect(mapStateToProps, mapActionsToProps)(injectIntl(App));
