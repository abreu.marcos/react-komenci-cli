import React from 'react';
import { Link } from 'react-router';
import { FormattedMessage } from 'react-intl';

const PageNotFound = () => {
  return (
    <div>
      <h1><FormattedMessage id="page-title.404" defaultMessage="Whoops! That was unexpected..." /></h1>
      <p>
        <FormattedMessage id="404.back-to-home" defaultMessage="Lets go back to the {homepage}"
          values={{
            homepage: <Link to="/"><FormattedMessage id="link.homepage" defaultMessage="homepage" /></Link>
          }}/>
      </p>
    </div>
  );
};

export default PageNotFound;
