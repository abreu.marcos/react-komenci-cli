import React from 'react';
import NavFooter from './nav-footer';

const AppFooter = () => {
  return (
    <div className="expanded row column">
      <NavFooter />
    </div>
  );
};

export default AppFooter;
