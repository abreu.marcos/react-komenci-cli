import React, { PropTypes } from 'react';
import NavTop from './nav-top';

const AppHeader = ({ switchLanguage, locale }) => {
  return (
    <NavTop
      switchLanguage={switchLanguage}
      locale={locale} />
  );
};

AppHeader.propTypes = {
  locale: PropTypes.string,
  switchLanguage: PropTypes.func
};

export default AppHeader;
