import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { FormattedMessage } from 'react-intl';

const NavTop = ({ switchLanguage, locale }) => {
  return (
    <div className="top-nav expanded row">
      <div>
        <Link to="/">
          <FormattedMessage id="navtop.home" defaultMessage="Home" />
        </Link>
      </div>
      <ul className="align-right">
        <li>
          <Link to="/about">
            <FormattedMessage id="navtop.about" defaultMessage="about" />
          </Link></li>
      </ul>

      <fieldset className="small-2 columns">
        <input type="radio" name="language" value="en"
                onChange={e => switchLanguage(e.currentTarget.value)}
                checked={locale === 'en'} /><label>English</label>
        <input type="radio" name="language" value="fr"
                onChange={e => switchLanguage(e.currentTarget.value)}
                checked={locale === 'fr'} /><label>Français</label>
      </fieldset>
    </div>
  );
};

NavTop.propTypes = {
  switchLanguage: PropTypes.func,
  locale: PropTypes.string
};

export default NavTop;
