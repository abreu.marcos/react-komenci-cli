import React from 'react';
import { FormattedRelative, FormattedMessage } from 'react-intl';
const PageAbout = () => {
  let date = new Date(1459913574887);
  return (
    <div>
      <h1><FormattedMessage id="page-title.about" defaultMessage="About" /></h1>
      <p><FormattedRelative value={date}/></p>
    </div>
  );
};

export default PageAbout;
