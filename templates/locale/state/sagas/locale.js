import { call, put } from 'redux-saga/effects';
import * as api from '../../lib/api';
import {
  loadLanguageSuccess,
  loadLanguageError,
  loadLanguageFailure,
  switchLanguage } from '../actions/locale';
import { appStatusSwitch, appMessage } from '../actions/app';

export default function* switchLanguageFlow(action) {
  let res;
  try {
    yield put(appStatusSwitch('busy'));
    res = yield call(api.loadLocaleData, action.locale);
    if (!res.error) {
      yield put(loadLanguageSuccess(res.locale, res.messages));
      yield put(switchLanguage(res.locale));
    }
    else {
      yield put(loadLanguageError(res.error));
    }
  }
  catch(e) {
    yield put(loadLanguageFailure(e.toString()));
  }
  finally {
    yield put(appStatusSwitch('free'));
  }

  if (res && res.error) { // todo: verify if needed
    yield put(appMessage(res.error));
  }
}
