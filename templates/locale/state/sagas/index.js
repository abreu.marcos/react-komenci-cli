import { fork } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import switchLanguageFlow from './locale';
import {
  LANGUAGE_SWITCH_REQUEST
} from '../constants/action-types';

// starts sagas in parallel as background tasks (demons)
export default function* sagas() {
  yield fork(takeEvery, LANGUAGE_SWITCH_REQUEST, switchLanguageFlow);
}
