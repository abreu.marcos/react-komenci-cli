export default {
  app: {
    message: '',
    status: 'free'
  },
  locale: {
    previous: 'en',
    locale: 'en',
    error: '',
    messages: {}
  }
};
