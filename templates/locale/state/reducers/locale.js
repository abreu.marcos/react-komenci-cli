// import objectAssign from 'object-assign'; // todo: replace with babel plugin
import initialState from './initial-state';
import {
  LANGUAGE_SWITCH,
  LANGUAGE_LOAD,
  LANGUAGE_LOAD_SUCCESS,
  LANGUAGE_LOAD_ERROR,
  LANGUAGE_LOAD_FAILURE
} from '../constants/action-types';

export default function localeReducer(state = initialState.locale, action) {
  switch(action.type) {
    case LANGUAGE_SWITCH:
      return Object.assign({}, state, {
        previous: state.locale,
        locale: action.locale
      });
    case LANGUAGE_LOAD:
      return state; // todo
    case LANGUAGE_LOAD_SUCCESS:
      return Object.assign({}, state, {
        messages: action.messages
      });
    case LANGUAGE_LOAD_ERROR:
    case LANGUAGE_LOAD_FAILURE:
      return Object.assign({}, state, {
        error: state.message,
        locale: state.previous
      });
    default:
      return state;
  }
}
