import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import appReducer from './app';
import localeReducer from './locale';

const rootReducer = combineReducers({
  app: appReducer,
  locale: localeReducer,
  routing: routerReducer
});

export default rootReducer;
