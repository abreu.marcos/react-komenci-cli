// app types
export const APP_STATUS_SWITCH = '@@app/APP_STATUS_SWITCH';
export const APP_MESSAGE = '@@app/APP_MESSAGE';

// locale action types
export const LANGUAGE_SWITCH_REQUEST = '@@app/LANGUAGE_SWITCH_REQUEST';
export const LANGUAGE_SWITCH = '@@app/LANGUAGE_SWITCH';
export const LANGUAGE_LOAD = '@@app/LANGUAGE_SWITCH_LOAD';
export const LANGUAGE_LOAD_SUCCESS = '@@app/LANGUAGE_LOAD_SUCCESS';
export const LANGUAGE_LOAD_ERROR = '@@app/LANGUAGE_LOAD_ERROR';
export const LANGUAGE_LOAD_FAILURE = '@@app/LANGUAGE_LOAD_FAILURE';
