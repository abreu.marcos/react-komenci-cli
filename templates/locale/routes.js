import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './containers/app';
import PageHome from './components/page-home';
import PageAbout from './components/page-about';
import PageNotFound from './components/page-not-found.js';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={PageHome} />
    <Route path="about" component={PageAbout} />
    <Route path="*" component={PageNotFound} />
  </Route>
);
