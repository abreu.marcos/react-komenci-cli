import Promise from 'bluebird';

export const login = (username, password) => {

  return Promise.resolve({token: 'fake-token', username, password}); // todo
  // return Promise.resolve({error: 'username not found in our database'}); // todo
  // return Promise.reject(new Error('server misbehaving'));
};

export const loadLocaleData = locale => {
  let messages;
  if (locale === 'fr') {
    messages = require('../../locales/fr.json');
  }
  else {
    messages = require('../../locales/en.json');
  }
  return Promise.resolve({ locale, messages });
};
