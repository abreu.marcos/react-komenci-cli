import { getStore } from '../index';

export const removeToken = () => {
  delete localStorage.token;
};

export const setToken = (token) => {
  if (token && typeof token === 'string') {
    localStorage.setItem('token', token);
  }
};

export const getToken = () => {
  return localStorage.token;
};

export const isAuthenticated = () => {
  const state = getStore().getState();
  return state.auth.isAuthenticated;
  // return (!!state.auth.token && !!state.auth.username);
};

/*
 * redirects user to new page if not authenitcated
 *    used as a callback handler for onEnter router events
 */
export const requireAuth = (nextState, replace) => {
  if (!isAuthenticated()) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    });
  }
};

export default {
  getToken,
  setToken,
  removeToken,
  isAuthenticated,
  requireAuth
};
