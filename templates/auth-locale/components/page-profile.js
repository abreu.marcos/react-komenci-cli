import React from 'react';
import { FormattedMessage } from 'react-intl';

const PageProfile = () => {
  return (
    <h1><FormattedMessage id="page-title.profile" defaultMessage="Profile" /></h1>
  );
};

export default PageProfile;
