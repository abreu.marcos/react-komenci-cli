import React from 'react';
import { FormattedMessage } from 'react-intl';

const PageHome = () => {
  return (
    <div>
      <h1><FormattedMessage id="page-title.home" defaultMessage="Komenci" /></h1>
      <p>ReactJS Start</p>
    </div>
  );
};

export default PageHome;
