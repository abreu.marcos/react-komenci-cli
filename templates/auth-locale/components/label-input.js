import React from 'react';

const LabelInput = ({labelClass, inputClass, browserHelp, children, error, ...props}) => {
  const spellCheck = (browserHelp == null || browserHelp);
  const autoComplete = spellCheck ? 'on' : 'off';
  const autoCorrect = autoComplete;
  const autoCapitalize = autoComplete;


  return (
    <div>
      <label htmlFor={props.id} className={(labelClass || '') + (error ? ' error' : '')}>{children}</label>
      <input type="text" className={(inputClass || '') + (error ? ' error' : '')} {...props}
             autoComplete={autoComplete} autoCorrect={autoCorrect} autoCapitalize={autoCapitalize} spellCheck={spellCheck} />
    </div>
  );
};

LabelInput.propTypes = {
  id: React.PropTypes.string.isRequired,
  labelClass: React.PropTypes.string,
  inputClass: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  maxLength: React.PropTypes.string,
  browserHelp: React.PropTypes.bool,
  onChange: React.PropTypes.func,
  error: React.PropTypes.node,
  value: React.PropTypes.string,
  children: React.PropTypes.node
};

export default LabelInput;
