import React from 'react';
import { Link } from 'react-router';
import { FormattedMessage } from 'react-intl';

const NavFooter = () => {
  return (
    <ul className="nav-footer">
      <li>
        <a href="https://www.capitalone.ca/identity-protection/privacy/" target="_blank">
          <FormattedMessage id="navfooter.privacy" defaultMessage="PRIVACY" />
        </a>
      </li>
      <li>
        <a href="https://www.capitalone.ca/identity-protection/commitment/" target="_blank">
          <FormattedMessage id="navfooter.security" defaultMessage="SECURITY" />
        </a>
      </li>
      <li>
        <Link to="terms-and-conditions" target="_blank">
          <FormattedMessage id="navfooter.terms" defaultMessage="TERMS &amp; CONDITIONS" />
        </Link>
      </li>
      <li>
        <a href="https://www.capitalone.ca/about/accessibility-commitment/" target="_blank">
          <FormattedMessage id="navfooter.accessibility" defaultMessage="ACCESSIBILITY" />
        </a>
      </li>
      <li>
        <Link to="faq">
          <FormattedMessage id="navfooter.faq" defaultMessage="FAQ" />
        </Link>
      </li>
    </ul>
  );
};

export default NavFooter;
