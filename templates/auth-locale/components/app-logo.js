import React, { PropTypes } from 'react';
import { intlShape } from 'react-intl';

const AppLogo = ({intl, locale}) => {
  const logoEN = require('../assets/images/logo-en.png');
  const logoFR = require('../assets/images/logo-fr.png');
  const logo = locale === 'en' ? logoEN : logoFR;

  return (
    <div className="app-logo">
      <img src={logo} alt={intl.formatMessage({id: 'image.logo', defaultMessage: 'komenci'})} />
    </div>
  );
};

AppLogo.propTypes = {
  intl: intlShape.isRequired,
  locale: PropTypes.string.isRequired
};

export default AppLogo;
