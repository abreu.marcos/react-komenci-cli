import React, { PropTypes } from 'react';
import NavTop from './nav-top';

const AppHeader = ({ isAuthenticated, cbLogout, switchLanguage, locale }) => {
  return (
    <NavTop
      isAuthenticated={isAuthenticated}
      cbLogout={cbLogout}
      switchLanguage={switchLanguage}
      locale={locale} />
  );
};

AppHeader.propTypes = {
  locale: PropTypes.string,
  isAuthenticated: PropTypes.bool,
  cbLogout: PropTypes.func,
  switchLanguage: PropTypes.func
};

export default AppHeader;
