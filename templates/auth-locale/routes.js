import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './containers/app';
import { requireAuth } from './lib/auth';
import PageHome from './components/page-home';
import PageLogin from './containers/page-login';
import PageAbout from './components/page-about';
import PageProfile from './components/page-profile';
import PageNotFound from './components/page-not-found.js';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={PageHome} />
    <Route path="login" component={PageLogin} />
    <Route path="about" component={PageAbout} />
    <Route path="profile" component={PageProfile} onEnter={requireAuth} />
    <Route path="*" component={PageNotFound} />
  </Route>
);
