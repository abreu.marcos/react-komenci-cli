import * as types from '../constants/action-types';

export const appStatusSwitch = status => {
  return {
    type: types.APP_STATUS_SWITCH,
    status
  };
};

export const appMessage = message => {
  return {
    type: types.APP_MESSAGE,
    message
  };
};
