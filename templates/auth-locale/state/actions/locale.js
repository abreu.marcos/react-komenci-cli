import * as types from '../constants/action-types';

export const switchLanguageRequest = locale => {
  return {
    type: types.LANGUAGE_SWITCH_REQUEST,
    locale
  };
};

export const switchLanguage = locale => {
  return {
    type: types.LANGUAGE_SWITCH,
    locale
  };
};

export const loadLanguage = locale => {
  return {
    type: types.LANGUAGE_LOAD,
    langId: locale
  };
};

export const loadLanguageSuccess = (locale, messages) => {
  return {
    type: types.LANGUAGE_LOAD_SUCCESS,
    locale,
    messages
  };
};

export const loadLanguageError = err => {
  return {
    type: types.LANGUAGE_LOAD_ERROR,
    message: err
  };
};

export const loadLanguageFailure = exception => {
  return {
    type: types.LANGUAGE_LOAD_FAILURE,
    message: exception
  };
};
