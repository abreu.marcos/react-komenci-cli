import { call, put, take, cancel, fork } from 'redux-saga/effects';
import * as api from '../../lib/api';
import { loginSuccess, loginError, loginFailure } from '../actions/auth';
import { appStatusSwitch, appMessage } from '../actions/app';
import {
  LOGIN_ERROR,
  LOGIN_FAILURE,
  LOGOUT_REQUEST
} from '../constants/action-types';

/*
 * this generator will authenticate the user using api requests
 */
function* authenticate(credentials) {
  try {
    yield put(appStatusSwitch('busy'));
    const res = yield call(api.login, credentials.username, credentials.password);
    if (!res.error) {
      yield put(loginSuccess(res.token, credentials.username));
    }
    else {
      yield put(loginError(res.error));
    }
  }
  catch(e) {
    yield put(loginFailure(e.toString()));
  }
  finally {
    yield put(appStatusSwitch('free'));
  }
}

/*
 * saga generator implementing the login/logout flow
 */
export default function* loginFlow(loginAction) {
  const authTask = yield fork(authenticate, loginAction.credentials);
  const action = yield take([LOGOUT_REQUEST, LOGIN_ERROR, LOGIN_FAILURE]);
  if (action.type === LOGOUT_REQUEST) {
    yield cancel(authTask);
  }
  else {
    yield put(appMessage(action.err));
  }
}
