import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import appReducer from './app';
import authReducer from './auth';
import localeReducer from './locale';

const rootReducer = combineReducers({
  app: appReducer,
  auth: authReducer,
  locale: localeReducer,
  routing: routerReducer
});

export default rootReducer;
