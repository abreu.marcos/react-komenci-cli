// import { expect } from 'chai';
// import _ from 'lodash';
// import * as ActionTypes from '../constants/action-types';
// import reducer from './access';
// import initialState from './initial-state';
//
// describe('Reducers::Access', () => {
//   // const getCredentials = () => {
//   //   return {
//   //     username: 'some user',
//   //     password: 'abcdef0001'
//   //   };
//   // };
//
//   const getAuthToken = () => {
//     return 'abcdefghijkl';
//   };
//
//   const initialStateClone = _.cloneDeep(initialState);
//
//   it('set initial state by default', () => {
//     const action = { type: 'UNKNOWN' };
//
//     expect(reducer(initialState, action)).to.deep.equal(initialState);
//     expect(initialState).to.deep.equal(initialStateClone);
//   });
//
//   it('handle LOG_IN action', () => {
//     const action = {
//       type: ActionTypes.LOG_IN,
//       authToken: getAuthToken()
//     };
//
//     const expected = _.cloneDeep(initialState);
//     expected.authToken = getAuthToken();
//     expect(reducer(initialState, action)).to.deep.equal(expected);
//     expect(initialState).to.deep.equal(initialStateClone);
//   });
//
//   it('handle LOG_OUT action', () => {
//     const action = {
//       type: ActionTypes.LOG_OUT
//     };
//     const expected = _.cloneDeep(initialState);
//     expected.authToken = null;
//     expect(reducer(initialState, action)).to.deep.equal(expected);
//     expect(initialState).to.deep.equal(initialStateClone);
//   });
// });
