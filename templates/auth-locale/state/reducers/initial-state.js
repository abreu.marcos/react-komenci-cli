export default {
  app: {
    message: '',
    status: 'free'
  },
  auth: {
    username: '',
    token: null,
    isAuthenticated: false,
    attempts: 0,
    time: null
  },
  locale: {
    previous: 'en',
    locale: 'en',
    error: '',
    messages: {}
  }
};
