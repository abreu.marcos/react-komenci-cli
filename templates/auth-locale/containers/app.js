import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { intlShape, injectIntl } from 'react-intl';

import * as authActionCreators from '../state/actions/auth';
import * as localeActionCreators from '../state/actions/locale';

import AppHeader from '../components/app-header';
import AppFooter from '../components/app-footer';
import { isAuthenticated } from '../lib/auth';
import IdleTimer from '../components/idle-timer';

/*
 * App Component - Root component of our application
 *                 include here the shared functionality of the app
 */
class App extends Component {

  // component properties
  static propTypes = {
    username: PropTypes.string,                         // state auth username
    token: PropTypes.string,                            // state auth token
    routing: PropTypes.object.isRequired,               // state routing
    locale: PropTypes.string.isRequired,
    switchLanguageRequest: PropTypes.func.isRequired,   // dispatch switchLanguage action
    logoutRequest: PropTypes.func.isRequired,           // dispatch logoutRequest action
    loginTarget: PropTypes.string,          // default url target when loging in
    logoutTarget: PropTypes.string,         // default url target when logging out
    timeout: PropTypes.number,              // login timeout in milliseconds
    intl: intlShape.isRequired,
    children: PropTypes.element             // element children
  }

  // default property values
  static defaultProps = {
    timeout: 10 * 60 * 1000,       // 10 minutes
    loginTarget: '/',
    logoutTarget: '/'
  }

  /**
   * Use the state properties changes to navigate after login or logout
   */
  componentWillReceiveProps = newProps => {
    const { token, routing, loginTarget, logoutTarget } = this.props;

    // Successifully Logged In - when going from NO token to token
    if (!token && newProps.token && newProps.username) {
      const location = routing.locationBeforeTransitions;
      const next = (location.state ? location.state.nextPathname || loginTarget : loginTarget);
      browserHistory.replace(next);
    }

    // Successifully Logged Out - when going from token to NO token
    else if (this.props.token && !newProps.token && !newProps.username) {
      browserHistory.push(logoutTarget);
    }
  }

  /**
   * Render App Shared Components
   */
  render() {
    const userAuthenticated = isAuthenticated();
    const { timeout, logoutRequest, locale, switchLanguageRequest, intl, children } = this.props;
    return (
      <IdleTimer timeout={timeout} action={logoutRequest}>
        <div className="expanded row column">
          <AppHeader
            locale={locale}
            cbLogout={logoutRequest}
            switchLanguage={switchLanguageRequest}
            isAuthenticated={userAuthenticated}
            intl={intl} />

          {children}

          <AppFooter
            isAuthenticated={userAuthenticated} />
        </div>
      </IdleTimer>
    );
  }
}

// link app state with component properties
const mapStateToProps = state => ({
  token: state.auth.token,
  username: state.auth.username,
  locale: state.locale.locale,
  routing: state.routing
});

// link app action creators with component properties
const mapActionsToProps = dispatch => {
  return bindActionCreators({
    logoutRequest: authActionCreators.logoutRequest,
    switchLanguageRequest: localeActionCreators.switchLanguageRequest
  }, dispatch);
};

export default connect(mapStateToProps, mapActionsToProps)(injectIntl(App));
