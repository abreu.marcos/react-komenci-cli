import React, { Component, PropTypes } from 'react';

/*
 * App Component - Root component of our application
 *                 include here the shared functionality of the app
 */
class App extends Component {

  // component properties
  static propTypes = {
    children: PropTypes.element             // element children
  }

  /**
   * Render App Shared Components
   */
  render() {
    return (
      <div className="expanded row column">
        {this.props.children}
      </div>
    );
  }
}

export default App;
