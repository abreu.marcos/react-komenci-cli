import React from 'react';
import { render } from 'react-dom';


// ### Static Global Assets
// ----------------------------------------------------------------------------
// import here the favicon
import './assets/styles/app.scss'; // root SASS style tree


// ### Routes
// ----------------------------------------------------------------------------
import { Router, browserHistory } from 'react-router';
import routes from './routes';


// ### Application Bootstrap
// ----------------------------------------------------------------------------
render(
  <Router history={browserHistory} routes={routes} />, document.getElementById('app')
);
