import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import AppLogo from '../components/app-logo';
import LabelInput from '../components/label-input';
import * as actionCreators from '../state/actions/auth';
import { validateLogin } from '../lib/validation';

class PageLogin extends Component {
  static propTypes = {
    token: PropTypes.string,            // state auth username
    username: PropTypes.string,         // state auth token
    routing: PropTypes.object,          // state routing
    errorMsg: PropTypes.string,         // generic error message
    loginRequest: PropTypes.func        // dispatch loginRequest action
  }

  state = {
    username: {
      value: '',
      error: null
    },
    password: {
      value: '',
      error: null
    },
    remember: false,
    good: false
  }

  /**
   * uses the component state information to attempt to log the user in
   */
  handleSubmit = (event) => {
    event.preventDefault();

    this.props.loginRequest({username: this.state.username.value, password: this.state.password.value});
    return false;
  }

  /**
   * store input changes as component state
   */
  handleChange = (event, validate) => {
    let newState = {};
    const key = event.target.id.replace('login-', '');
    const value = (key === 'remember' ? event.target.checked : event.target.value);

    if (key === 'remember') {
      newState[key] = value;
    }
    else {
      newState[key] = {};
      newState[key].value = value;
      newState[key].error = validate(key, value);
    }

    // todo: this is still changing the original state
    newState = Object.assign({}, this.state, newState);
    newState.good = (newState.username.error == null && newState.username.value.length > 0 &&
                     newState.password.error == null && newState.password.value.length > 0);
    this.setState(newState);
  }

  /*
   * Render html component structure
   */
  render() {
    return (
      <div>
        <AppLogo />
        <form onSubmit={this.handleSubmit}>
          <fieldset>
            {/* username */}
            <LabelInput id="login-username" browserHelp={false} maxLength="50"
              placeholder="username"
              error={this.state.username.error}
              value={this.state.username.value}
              labelClass="show-for-sr"
              onChange={(e) => this.handleChange(e, validateLogin)}>Username</LabelInput>

            {/* password */}
            <LabelInput id="login-password" browserHelp={false} maxLength="50" type="password"
              placeholder="password"
              error={this.state.password.error}
              value={this.state.password.value}
              labelClass="show-for-sr"
              onChange={(e) => this.handleChange(e, validateLogin)}>Password</LabelInput>
            <p className="help-text">passwords should have at least 8 characters</p>

            {/* Remember Me */}
            <input type="checkbox" id="login-remember"
              onChange={(e) => this.handleChange(e, ()=>{})}
              checked={this.state.remember} />
            <label>Remember me</label>

            {/* Submit */}
            <input type="submit" className="button" disabled={!this.state.good} value="Sign In" />
          </fieldset>
          {this.props.errorMsg && (
            <p className="error">Bad login credentials</p>
          )}
        </form>
      </div>
    );
  }
}

const mapActionCreatorsToProps = dispatch => {
  return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = state => ({
  token: state.auth.token,
  username: state.auth.username,
  routing: state.routing
});


export default connect(mapStateToProps, mapActionCreatorsToProps)(PageLogin);
