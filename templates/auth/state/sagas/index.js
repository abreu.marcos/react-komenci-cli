import { fork } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import loginFlow from './auth';
import {
  LOGIN_REQUEST
} from '../constants/action-types';

// starts sagas in parallel as background tasks (demons)
export default function* sagas() {
  yield fork(takeEvery, LOGIN_REQUEST, loginFlow);
}
