import * as types from '../constants/action-types';

export const loginRequest = credentials => {
  return {
    type: types.LOGIN_REQUEST,
    credentials
  };
};

export const loginSuccess = (token, username) => {
  return {
    type: types.LOGIN_SUCCESS,
    token,
    username
  };
};

export const loginError = err => {
  return {
    type: types.LOGIN_ERROR,
    err
  };
};

export const loginFailure = err => {
  return {
    type: types.LOGIN_FAILURE,
    err
  };
};

export const logoutRequest = () => {
  return {
    type: types.LOGOUT_REQUEST
  };
};
