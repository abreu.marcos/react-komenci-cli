// app types
export const APP_STATUS_SWITCH = '@@app/APP_STATUS_SWITCH';
export const APP_MESSAGE = '@@app/APP_MESSAGE';

// auth action types
export const LOGIN_REQUEST  = '@@app/LOGIN_REQUEST';
export const LOGIN_SUCCESS  = '@@app/LOGIN_SUCCESS';
export const LOGIN_ERROR    = '@@app/LOGIN_ERROR';
export const LOGIN_FAILURE  = '@@app/LOGIN_FAILURE';
export const LOGIN_TIMEOUT  = '@@app/LOGIN_TIMEOUT';
export const LOGOUT_REQUEST = '@@app/LOGOUT_REQUEST';
