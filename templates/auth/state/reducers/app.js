import objectAssign from 'object-assign'; // todo: replace with babel plugin
import initialState from './initial-state';
import {
  APP_STATUS_SWITCH,
  APP_MESSAGE
} from '../constants/action-types';

export default function globalReducer(state = initialState.app, action) {
  switch(action.type) {
    case APP_STATUS_SWITCH:
      return objectAssign({}, state, {
        status: action.status
      });
    case APP_MESSAGE:
      return objectAssign({}, state, {
        message: action.message
      });
    default:
      return state;
  }
}
