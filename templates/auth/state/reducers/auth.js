import objectAssign from 'object-assign'; // todo: replace with babel plugin
import initialState from './initial-state';
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_FAILURE,
  LOGOUT_REQUEST
} from '../constants/action-types';

const logout = (state) => {
  return objectAssign({}, state, {
    token: null,
    username: '',
    attempts: 0,
    isAuthenticated: false
  });
};

export default function authReducer(state = initialState.auth, action) {
  switch(action.type) {
    case LOGIN_REQUEST:
      return state;
    case LOGIN_SUCCESS:
      return objectAssign({}, state, {
        username: action.username,
        isAuthenticated: true,
        token: action.token,
        attempts: 0
      });
    case LOGIN_ERROR:
      return objectAssign({}, logout(state), {
        attempts: (state.attempts + 1)
      });
    case LOGIN_FAILURE:
      // if our server is misbehaving then reset the attempts
      return objectAssign({}, logout(state), {
        attempts: 0
      });
    case LOGOUT_REQUEST:
      return logout(state);
    default:
      return state;
  }
}
