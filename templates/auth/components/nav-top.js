import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const NavTop = ({ cbLogout, isAuthenticated }) => {
  return (
    <div className="top-nav expanded row">
      <div>
        <Link to="/">Home</Link>
      </div>
      <ul className="align-right">
      {!isAuthenticated && (
        <li>
          <Link to="/login">Login</Link>
        </li>
      )}
      {isAuthenticated && (
        <li>
          <a href="javascript: void(0)" onClick={() => { cbLogout(); return false; }}>Log out</a>
        </li>
      )}
      {isAuthenticated && (
        <li>
          <Link to="/profile">Profile</Link>
        </li>
      )}
        <li>
          <Link to="/about">about</Link></li>
      </ul>
    </div>
  );
};

NavTop.propTypes = {
  isAuthenticated: PropTypes.bool,
  cbLogout: PropTypes.func
};

export default NavTop;
