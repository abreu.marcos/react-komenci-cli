import React, { PropTypes } from 'react';
import NavTop from './nav-top';

const AppHeader = ({ isAuthenticated, cbLogout }) => {
  return (
    <NavTop
      isAuthenticated={isAuthenticated}
      cbLogout={cbLogout} />
  );
};

AppHeader.propTypes = {
  isAuthenticated: PropTypes.bool,
  cbLogout: PropTypes.func
};

export default AppHeader;
