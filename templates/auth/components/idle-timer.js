import React, { Component, PropTypes } from 'react'; // eslint-disable-line no-unused-vars

export default class IdleTimer extends Component {
  static propTypes = {
    timeout: PropTypes.number, // timeout in milliseconds
    element: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // element to watch for
    events: PropTypes.arrayOf(PropTypes.string), // array of event strings to watch for
    action: PropTypes.func, // action to call when element becomes inactive
    startOnLoad: PropTypes.bool, // starts the timer onLoad browser event
    children: PropTypes.element
  }

  static defaultProps = {
    timeout: 5 * 60 * 1000, // 10 minutes
    element: document,
    events: ['mousemove', 'keydown', 'wheel', 'DOMMouseScroll', 'mouseWheel',
             'mousedown', 'touchstart', 'touchmove', 'MSPointerDown', 'MSPointerMove'],
    action: () => {},
    startOnLoad: true
  }

  state = {
    timerId: null,
    lastActive: Date.now(),
    pageX: null,
    pageY: null
  }

  componentWillMount() {
    this.props.events.forEach(e => this.props.element.addEventListener(e, this.handler));
  }

  componentDidMount() {
    if (this.props.startOnLoad) {
      this.reset();
    }
  }

  componentWillUnmount() {
    clearTimeout(this.state.timerId);
    this.props.events.forEach(e => this.props.element.removeEventListener(e, this.handler));
  }

  handler = evt => {
    // mousemove event
    if (evt.type === 'mousemove') {
      // if coord are same, it didn't move
      if (evt.pageX === this.state.pageX && evt.pageY === this.state.pageY) {
        return;
      }

      // if coord don't exist how could it have moved
      if (typeof evt.pageX === 'undefined' && typeof evt.pageY === 'undefined') {
        return;
      }

      // under 200 ms is hard to do, and you would have to stop, as continuous
      // activity will bypass this
      const elapsed = (+new Date()) - this.state.oldDate;
      if (elapsed < 200) {
        return;
      }
    }

    // clear any existing timeout
    clearTimeout(this.state.timerId);

    this.setState(Object.assign({}, this.state, {
      lastActive: Date.now(), // when was the element last active
      pageX: evt.pageX, // mouse coordinates from event
      pageY: evt.pageY, // mouse coordinates from event
      timerId: setTimeout(this.props.action, this.props.timeout) // set a new timeout
    }));
  }

  reset = () => {
    // reset timers
    clearTimeout(this.state.timerId);

    // reset settings
    this.setState(Object.assign({}, this.state, {
      lastActive: Date.now(),
      pageX: null,
      pageY: null,
      timerId: setTimeout(this.props.action, this.props.timeout)
    }));
  }

  render() {
    return this.props.children ? this.props.children : null;
  }
}
