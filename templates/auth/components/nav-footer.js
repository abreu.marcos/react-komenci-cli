import React from 'react';
import { Link } from 'react-router';

const NavFooter = () => {
  return (
    <ul className="nav-footer">
      <li>
        <a href="https://www.capitalone.ca/identity-protection/privacy/" target="_blank">PRIVACY</a>
      </li>
      <li>
        <a href="https://www.capitalone.ca/identity-protection/commitment/" target="_blank">SECURITY</a>
      </li>
      <li>
        <Link to="terms-and-conditions">TERMS &amp; CONDITIONS</Link></li>
      <li>
        <a href="https://www.capitalone.ca/about/accessibility-commitment/" target="_blank">ACCESSIBILITY</a>
      </li>
      <li>
        <Link to="faq">FAQ</Link>
      </li>
    </ul>
  );
};

export default NavFooter;
