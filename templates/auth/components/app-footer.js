import React, { PropTypes } from 'react';
import NavFooter from './nav-footer';

const AppFooter = () => {
  return (
    <div className="expanded row column">
      <NavFooter />
    </div>
  );
};

AppFooter.propTypes = {
  isAuthenticated: PropTypes.bool
};

export default AppFooter;
