export const validateLogin = (key, value) => {
  if (!value || value.length === 0) {
    return null; // nothing to validate
  }

  switch(key) {
    case 'username':
      if (!/[a-zA-Z]/.test(value[0])) {
        return 'username needs to start with a letter';
      }
      else if (value.length < 4) {
        return 'username requires at least 4 characters';
      }

      break;
    case 'password':
      if (value.length < 8) {
        return 'passwords should have at least 8 characters';
      }
      break;
  }
};
