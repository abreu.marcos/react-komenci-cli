import React from 'react';
import { render } from 'react-dom';


// ### Static Global Assets
// ----------------------------------------------------------------------------
import './assets/images/favicon.ico'; // webpack to load favicon.ico
import './assets/styles/app.scss'; // root SASS style tree


// ### Routes
// ----------------------------------------------------------------------------
import { Router, browserHistory } from 'react-router';
import routes from './routes';


// ### Redux (application state) and Sagas (asynchrounous flows)
// ----------------------------------------------------------------------------
import { Provider } from 'react-redux';
import sagas from './state/sagas';
import createSagaMiddleware from 'redux-saga';
import configureStore from './state/store/configure-store'; // eslint-disable-line import/default
const sagaMiddleware = createSagaMiddleware();
const store = configureStore(undefined, sagaMiddleware);
sagaMiddleware.run(sagas);


// ### Browser History + Redux
// ----------------------------------------------------------------------------\
import { syncHistoryWithStore } from 'react-router-redux';
const history = syncHistoryWithStore(browserHistory, store); // syncs history with store


// ### Application Bootstrap
// ----------------------------------------------------------------------------

render(
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>, document.getElementById('app')
);

// ### Expose Methods
// ----------------------------------------------------------------------------

// expose the store to other modules, allowing non-components to access it
export function getStore() {
  return store;
}
